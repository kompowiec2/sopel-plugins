# coding=utf-8
# https://pypi.org/project/wikiquote/
# https://pypi.org/project/sopel/
# Author xmszkn(at)disroot.org
# GPLv3 license

from sopel import plugin, tools # sopel modules
import wikiquote # from wikiquote
import random

@plugin.command('cytat')
@plugin.action_command('cytat')
@plugin.example('.cytat', r'¯\_(ツ)_/¯')

def cytat(bot, trigger):
    query = trigger.group(2)
    if not query:
        bot.reply('Podaj zapytanie (autor, pojęcie)')
        return
    autor_znal = wikiquote.search(query, lang='pl') # autorzy jako lista
    if len(autor_znal) == 0: # brak autora
        bot.say('Nie ma cytatu odpowiadającego twojemu zapytaniu.')
        return
    autor_def = (autor_znal[0]) # autor pierwsza pozycja z listy
    cytat = wikiquote.quotes((autor_def), lang='pl') # szukanie cytatu jesli brak to
    if len(cytat) == 0:
        bot.say('Nie ma cytatu odpowiadającego twojemu zapytaniu.')
        return
    answer = random.choice(wikiquote.quotes((autor_def), lang='pl')) # wlasciwy cytat
    bot.say(answer) # odpowiedz bota