#Sopel Irc Plugin
#GPL 3.0
#author xmszkn@disroot.org

from sopel import module

#rule calling, regex
@module.rule('.*(https?://(?:www\.)?(?:youtube\.com/watch\?v=|youtu\.be/)([\w-]+)).*')
#method definition
def convert_to_piped(bot, trigger):
    yt_url = trigger.group(1)
    if 'youtu.be' in yt_url: #cutting the film identifier from the youtube.com or youtu.be url
        yt_video_id = yt_url.split('/')[-1]
    else:
        yt_video_id = yt_url.split('=')[-1]
    piped_url = f'http://piped.video/watch?v={yt_video_id}'
    if 'watch' in yt_video_id:
        return None
    bot.say("Piped URL: " + piped_url) #bot sends a message to the channel on which the youtube.com link appeared

