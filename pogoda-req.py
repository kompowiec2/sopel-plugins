# -*- coding: UTF-8 -*-
# Author xmszkn
# script for Sopel Bot
# working with pybot on #knajpa
# use wttr.in
# https://github.com/chubin/wttr.in

# import modules, You need to install the following from pypi.org (pip install...): ushlex, bs4, requests and lxml
from sopel import plugin, tools
from shlex import quote
import requests
from bs4 import BeautifulSoup
# calling commands and triggers
@plugin.command('pogoda', 'p')
@plugin.action_command('pogoda')
@plugin.example('.pogoda Londyn', 'Londyn: ⛅️  🌡️+11°C 🌬️↗26km/h')
# method definition
def Pogoda(bot, trigger):
    query = trigger.group(2)
    if not query:
        bot.reply('Podaj miasto. np. .pogoda Londyn')
    else:
        query = query.strip()
        start_url = ("https://wttr.in/" + quote(query) + "?format=%l:+%c+%t+%h+%w+%P+%m+uv:%u")
        download_html = requests.get(start_url)
        soup = BeautifulSoup(download_html.text, features="lxml")
        pogoda_raw = str(soup)
        pogoda_raw_1 = pogoda_raw.lstrip("<html><body><p>")
        pogoda = pogoda_raw_1.rstrip("</p></body></html>")
        exclude = "404"
        if exclude in pogoda:
            bot.say("Nie ma takiego miasta. Jest Lądek.. Lądek-Zdrój")
        else:
            bot.say(pogoda)
