# coding=utf-8
# https://pypi.org/project/sopel/
# Author xmszkn(at)disroot.org
# GPLv3 license
# title: Pope's script commemorates the hour of death

from datetime import datetime
from sopel import plugin

SECOND = 1 # definition of a second
CHANNEL = "#python" # definition of irc channel to send a message
last_sent_date = None  # date of last message
already_sent = None  # check if sent 


@plugin.interval(SECOND*5) # loop every five seconds
def p2157(bot):
    global last_sent_date  # sets as global variables
    global already_sent
    
    today = datetime.now()

    # checks if it is a new day
    if last_sent_date is None or last_sent_date.date() != today.date():
        last_sent_date = datetime.now()
        already_sent = False

    # sending only once at 21:37
    if already_sent is False and today.hour == 21 and today.minute == 37:
        bot.say("2137", recipient=CHANNEL)
        already_sent = True


