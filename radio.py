import requests
import sopel
from sopel import tools
from sopel.module import commands

@sopel.module.commands('radio')
def radio(bot, trigger):
    # get arguments from command
    args = tools.get_args(trigger)
    if len(args) < 2:
        bot.say("Usage: .radio [youtube_link] [radio_station_name_or_id]")
        return
    youtube_link = args[0]
    radio_station = args[1]

    # get youtube title using youtube-dl
    youtube_title = sopel.tools.get_url_title(youtube_link)

    # get icecast server info from configuration file
    icecast_host = "localhost" # change this to your icecast host
    icecast_port = 8000 # change this to your icecast port
    icecast_user = "source" # change this to your icecast user
    icecast_pass = "hackme" # change this to your icecast password

    # play youtube audio using youtube-dl and ffmpeg
    bot.say("Playing {} on {}".format(youtube_title, icecast_mount))
    os.system("youtube-dl --extract-audio --exec \"ffmpeg -i - -acodec libmp3lame -ab 128k -f mp3 http://{}:{}@{}:{}/{}\" {}".format(icecast_user, icecast_pass, icecast_host, icecast_port, icecast_mount, youtube_link))