# coding=utf-8
# https://pypi.org/project/sopel/
# Author xmszkn(at)disroot.org
# GPLv3 license

from sopel import plugin # sopel modules
from random import choice

@plugin.command('memphis', 'm', 'memph')
@plugin.action_command('mephis')
@plugin.example('.mephis', r'memphis')

def cytat(bot, trigger):
    query = trigger.group(2)
    if not query:
    # list with phrases from file about memphis
        with open('for_example/sciezka/hejty.txt', 'r') as plik:
            hejty = plik.readlines()
    # random choice from list
        answer = choice(hejty)
        bot.say(answer)
    if query == "add":
        bot.reply("ta funkcjonalność jest w budowie")